package main

import (
	"gitlab.com/vlaborie/remarkable-sync/cmd"
)

func main() {
	cmd.Execute()
}
